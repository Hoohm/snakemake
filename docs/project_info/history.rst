.. project_info-history:

.. _changelog:

==========
Change Log
==========

.. literalinclude:: ../../CHANGELOG.md
    :language: text
